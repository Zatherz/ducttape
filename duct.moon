#!/usr/bin/moon
DuctTapeInterpreter = require "interpreter"

interpreter = DuctTapeInterpreter
	    debug_enabled: arg[2] == "true"
	    input_enabled: not arg[3]

if not arg[1]
	error "At least one argument needed!"

io.write interpreter\run arg[1], arg[3]
