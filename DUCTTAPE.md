# Duct Tape language documentation

## Tapes
* `main`
* `alt`
* `result`

## Commands
### Tape control
* `'` - switches the current tape to `main`
* `"` - switches the current tape to `alt`
* `^` - switches the current tape to `result`
* `p` - push the current value of the current tape to the stack of the current tape, each tape gets its own stack
* `P` - pop the top of the current tape's stack into the current tape

### Math
* `+` - adds 1 to the current tape
* `-` - subtracts 1 from the current tape
* `x` - adds 10 to the current tape
* `/` - subtracts 10 from the current tape

### Input/Output
* `.` - print the current value of the current tape as an ASCII character of that numeric value
* `,` - print the current value of the current tape as a number
* `:` - accept one ASCII character of input (has to be confirmed with Enter, will let you type in more than one character but only the first one will be used) and set the value of the current tape to its numeric representation
* `;` - accept an input that's a valid number (will error otherwise) and set the value of the current tape to that number
* `?` - print `debug!`

### Loops
* `(` - jump to the character right after the closest `)` after this character if the current value of the register is equal to 0
* `)` - jump to the closest `(` before this character if the current value of the register is bigger than 0
* `{` - jump to the character right after the closest `}` after this character if the current value of the register is bigger than 0
* `}` - jump to the closest `}` before this character if the current value of the register is equal to 0

### Program control
* `!` - prematurely exit the program

## Quirks

### Loops

The `(`, `)`, `{` and `}` characters are *commands*, not any kind of special syntax. They operate on the current tape, which means that you will need to set the tape back to continue properly looping if you changed the current tape inside the loop.  
Example: `'+++"+'(,-"-)` will print only `3` instead of the expected output of `3`, `2` and `1` on separate lines.  
This is because when the interpreter hits the `)`, the tape is still set to the `alt` tape. Since that tape was just subtracted by 1, and its value was 1, it now equals 0.  
The `)` command notices this, and does not go back to the beginning of the loop. To get the expected result, switch the tape before the `)` command is executed: `'+++"+'(,-"-')`.  
This prints the expected result.
