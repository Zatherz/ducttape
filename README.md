# Duct Tape

An esoteric language based on the idea of tapes.  
You have 3 tapes to your disposition: `main` (`'`), `alt` (`"`) and `result` (`^`).  
You can do various operations on these tapes: add/subtract 1, add/subtract 10, push them to their own special stack, pop them from their stack, etc.

Full documentation on the language is available in DUCTTAPE.md.

# Running

Install MoonScript, then run `moon duct.moon <code> ['true' for debug mode] [fake input]`

You can use anything other than `true` for the second argument to disable debug mode if you want to provide the fake input.  
If `fake input` is provided, instead of asking the user to interactively provide input, the fake input will be used whenever `:` or `;` is used.

# Examples

* `p{:,Pp}` - Repeatedly asks you to input one ASCII character then prints its numeric representation
* `^x';p ";p '(-"+')'P",^."P,^.'',^.` - Gets two numbers, adds them together, prints the output, then prints the two components.
* `xxxxxxx++.-xxx.x---..+++.(-)xxx++.xxxxxxxxx---./++.+++./++++./++.(-)xxx+++.(-)x.` - Hello world!
