default = (value, default) ->
	if value == nil
		return default
	value

class DuctTapeInterpreter
	debug_enabled: false
	tape_max: 4294967295
	tapes:
		main: (real: 0, stack: {})
		alt: (real: 0, stack: {})
		result: (real: 0, stack: {})
	current_tape: "main"
	input_enabled: true

	get_value: (tape) => @tapes[tape].real
	set_value: (tape, val) =>
		realval = val
		if val < 0
			realval = @tape_max + val
		@tapes[tape].real = realval
	add_value:  (tape, val) => @set_value tape, @tapes[tape].real + val
	push_value: (tape) => table.insert @tapes[tape].stack, @tapes[tape].real
	pop_value:  (tape) =>
		@tapes[tape].real = @tapes[tape].stack[#@tapes[tape].stack] or 0
		@tapes[tape].stack[#@tapes[tape].stack] = nil
	debug: (info, msg) =>
		if @debug_enabled
			print "[debug:" .. (info or "?") .. "] " .. msg
			

	loop_starts: {}
	loop_starts_0: {}
	skip_until_rparen: false
	skip_until_rparen_0: false

	new: (opts) =>
		opts = opts or {}
		@debug_enabled = default opts.debug_enabled, @debug_enabled
		@tape_max = default opts.tape_max, @tape_max
		@tapes = default opts.tapes, @tapes
		@current_tape = default opts.initial_tape, @current_tape
		@input_enabled = default opts.input_enabled, @input_enabled

	run: (input, fake_input) =>
		ret = ""
		i = 0
		while i <= #input
			i += 1
			c = input\sub i, i
		
		
			if c == "("
				if (@get_value @current_tape) > 0
					@debug tostring(i) .. ":" .. c, "Inserting a new >0 loop start (#" .. (#@loop_starts + 1) .. ")"
					table.insert @loop_starts, i
				else
					@debug tostring(i) .. ":" .. c, "Skipping until right >0 parenthesis"
					@skip_until_rparen = true
			elseif c == ")"
				if @skip_until_rparen
					@debug tostring(i) .. ":" .. c, "Finished skipping"
					@skip_until_rparen = false
					continue
				elseif (@get_value @current_tape) > 0
					@debug tostring(i) .. ":" .. c, "Going back to last loop start #" .. #@loop_starts .. " @ " .. (@loop_starts[#@loop_starts] or "[none]")
					i = @loop_starts[#@loop_starts] - 1
					@loop_starts[#@loop_starts] = nil
				else
					@debug tostring(i) .. ":" .. c, ">0 loop ending!"
					@loop_starts[#@loop_starts] = nil
			elseif c == "{"
				if (@get_value @current_tape) == 0
					@debug tostring(i) .. ":" .. c, "Inserting a new =0 loop start #" .. #@loop_starts_0 .. " @ " .. (@loop_starts_0[#@loop_starts_0] or "[none]")
					table.insert @loop_starts_0, i
				else
					@debug tostring(i) .. ":" .. c, "Skipping until right =0 parenthesis"
					@skip_until_rparen_0 = true
			elseif c == "}"
				if @skip_until_rparen_0
					@debug tostring(i) .. ":" .. c, "Finished skipping"
					@skip_until_rparen_0 = false
				elseif (@get_value @current_tape) == 0
					@debug tostring(i) .. ":" .. c, "Going back to last loop start #" .. #@loop_starts_0 .. " @ " .. (@loop_starts_0[#@loop_starts_0] or "[none]")
					i = @loop_starts_0[#@loop_starts_0] - 1
					@loop_starts_0[#@loop_starts_0] = nil
				else
					@debug tostring(i) .. ":" .. c, "=0 loop ending!"
					@loop_starts[#@loop_starts] = nil
			elseif not @skip_until_rparen and not @skip_until_rparen_0
				if c == ":"
					inputascii = nil
					if @input_enabled
						@debug tostring(i) .. ":" .. c, "Getting ASCII input"
						io.write ">"
						inputascii = io.read!
					else
						if fake_input
							@debug tostring(i) .. ":" .. c, "Faking ASCII input"
							inputascii = fake_input
					@set_value @current_tape, string.byte(inputascii\sub 1, 1)
				elseif c == ";"
					inputnum = nil
					if @input_enabled
						@debug tostring(i) .. ":" .. c, "Getting number input"
						io.write ">"
						inputnum = tonumber io.read!
					else
						if fake_input
							@debug tostring(i) .. ":" .. c, "Faking number input"
						inputnum = tonumber fake_input
					if not inputnum
						error "Input to ';' wasn't a number!"
					@set_value @current_tape, inputnum
				elseif c == "'"
					@debug tostring(i) .. ":" .. c, "Switching to main tape"
					@current_tape = "main"
				elseif c == "\""
					@debug tostring(i) .. ":" .. c, "Switching to alt tape"
					@current_tape = "alt"
				elseif c == "^"
					@debug tostring(i) .. ":" .. c, "Switching to result tape"
					@current_tape = "result"
				elseif c == "p"
					@debug tostring(i) .. ":" .. c, "Pushing value of tape " .. @current_tape .. " (" .. (@get_value @current_tape) .. ") to its stack"
					@push_value @current_tape
				elseif c == "P"
					@debug tostring(i) .. ":" .. c, "Popping value of " .. @current_tape .. " tape's stack into the tape"
					@pop_value @current_tape
				elseif c == "+"
					@debug tostring(i) .. ":" .. c, "Adding 1 to tape " .. @current_tape
					@add_value @current_tape, 1
				elseif c == "x"
					@debug tostring(i) .. ":" .. c, "Adding 10 to tape " .. @current_tape
					@add_value @current_tape, 10
				elseif c == "-"
					@debug tostring(i) .. ":" .. c, "Subtracting 1 from tape " .. @current_tape
					@add_value @current_tape, -1
				elseif c == "/"
					@debug tostring(i) .. ":" .. c, "Subtracting 10 from tape " .. @current_tape
					@add_value @current_tape, -10
				elseif c == "."
					@debug tostring(i) .. ":" .. c, "Printing value of tape " .. @current_tape .. " (" .. (@get_value @current_tape) .. ") in ASCII"
					ret ..= string.char (@get_value @current_tape)
				elseif c == ","
					@debug tostring(i) .. ":" .. c, "Printing value of tape " .. @current_tape .. " (" .. (@get_value @current_tape) .. ")"
					ret ..= @get_value @current_tape
				elseif c == "?"
					@debug tostring(i) .. ":" .. c, "Debug request"
					print "debug!"
				elseif c == "!"
					@debug tostring(i) .. ":" .. c, "Quitting!"
					return
		if @skip_until_rparen or #@loop_starts > 0
			error "Missing right parenthesis!"
		ret
